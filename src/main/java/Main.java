import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        HSSFWorkbook readBook = null;
        try (FileInputStream stream = new FileInputStream(new File("stroiman.ru.xls"))) {
            readBook = new HSSFWorkbook(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        HSSFSheet readSheet = readBook.getSheet("Builders");
        HSSFRow firstRow = readSheet.getRow(0);

        HSSFWorkbook writeBook = new HSSFWorkbook();
        Sheet sheet = writeBook.createSheet("Builders");
        Row row = sheet.createRow(0);
        for (int i = 0;  i < firstRow.getLastCellNum(); i++)
        row.createCell(i).setCellValue(firstRow.getCell(i).getStringCellValue());

        Scanner scan = new Scanner(System.in);
        System.out.println("Input a word to search:");
        String word = scan.nextLine().toLowerCase();
        ArrayList<HSSFRow> rows = getByWord(readSheet, word);
        rows = getByLocations(rows, inputLocations());
        for (int i = 0; i<rows.size(); i++){
            Row rowToWrite = sheet.createRow(i+1);
            for (int j = 0; j<=rows.get(i).getLastCellNum(); j++){
                String toCell = "";
                if (rows.get(i).getCell(j)!=null) {
                    toCell = rows.get(i).getCell(j).getStringCellValue();
                }
                rowToWrite.createCell(j).setCellValue(toCell);
            }
        }

        sheet.setColumnWidth(0,6000);
        sheet.setColumnWidth(1,6000);
        sheet.setColumnWidth(2,4000);
        sheet.setColumnWidth(3,15000);
        sheet.setColumnWidth(4,4000);
        sheet.setColumnWidth(5,8000);
        CellStyle style = writeBook.createCellStyle();
        style.setWrapText(true);
        for (Row row1 : sheet){
            row1.setHeight((short)-1);
        }

        System.out.println("Input new file name:");
        String file = scan.nextLine()+".xls";
        try(FileOutputStream fos = new FileOutputStream(file))  {
            writeBook.write(fos);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static ArrayList<HSSFRow> getByWord(HSSFSheet readSheet, String word){
        ArrayList<HSSFRow> rows = new ArrayList<HSSFRow>();
        for (int i = 1; i <= readSheet.getLastRowNum(); i++){
            if (readSheet.getRow(i).getCell(3) != null && readSheet.getRow(i).getCell(3).getStringCellValue().toLowerCase().contains(word)){
                rows.add(readSheet.getRow(i));
            }
        }
        return rows;
    }

    private static ArrayList<HSSFRow> getByLocations(ArrayList<HSSFRow> rows, String[] locations){
        if (locations[0].equals("all")){
            return rows;
        }
        ArrayList<HSSFRow> newRows = new ArrayList<>();
        for (HSSFRow row : rows){
            for (String location : locations){
                if (row.getCell(0).getStringCellValue().toLowerCase().contains(location)){
                    newRows.add(row);
                }
            }
        }
        return newRows;
    }

    private static String[] inputLocations(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Input region-words to search in or \"all\" to search in all regions:");
        String read = scan.nextLine().toLowerCase();
        String[] locations = read.split("[\";,\\s]+");
        return locations;
    }

}
